package com.gitee.sunchenbin.mybatis.actable.command.sqlite;

public class SysSqliteTable {

    /**
     * 表注释
     */
    /** 字符集的后缀 */
    public static final String TABLE_COLLATION_SUFFIX = "_general_ci";
    /** 字符集 */
    public static final String TABLE_COLLATION_KEY = "table_collation";
    /** 注释 */
    public static final String TABLE_COMMENT_KEY = "table_comment";

    private String name;
    private String type;
    private String table_name;
    private Long rootpage;
    private String sql;
    private String table_comment;

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getTable_comment() {
        return table_comment;
    }

    public void setTable_comment(String table_comment) {
        this.table_comment = table_comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getRootpage() {
        return rootpage;
    }

    public void setRootpage(Long rootpage) {
        this.rootpage = rootpage;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }
}
