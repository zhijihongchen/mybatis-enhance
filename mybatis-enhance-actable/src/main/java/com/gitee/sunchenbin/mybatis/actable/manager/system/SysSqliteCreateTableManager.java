package com.gitee.sunchenbin.mybatis.actable.manager.system;


import java.util.List;

/**
 * @description
 * @author hanpeng
 * @date 2022/7/23 23:26
 */
public interface SysSqliteCreateTableManager {

    /**
     * 创建sqlite表
     */
    void createSqliteTable();

    /**
     * 获取所有字段
     *
     * @param clas 类实例
     * @return @return {@link List }<{@link Object }>
     */
    List<Object> getAllFields(Class<?> clas);

}
