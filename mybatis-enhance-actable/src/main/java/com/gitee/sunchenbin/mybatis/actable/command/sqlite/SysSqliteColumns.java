package com.gitee.sunchenbin.mybatis.actable.command.sqlite;

/**
 * 用于查询表中字段结构详细信息
 * @author hanpeng
 * @date 2022/7/23 23:39
 */
public class SysSqliteColumns {

	/**
	 * cid
	 */
	public static final String CID_KEY = "cid";
	/**
	 * 字段名
	 */
	public static final String NAME_KEY = "name";
	/**
	 * 类型
	 */
	public static final String TYPE_KEY = "type";
	/**
	 *
	 */
	public static final String NOTNULL_KEY = "notnull";
	/**
	 * 长度，如果是0的话是null
	 */
	public static final String DFLT_VALUE_KEY = "dflt_value";
	/**
	 * 是否为主键，是的话是PRI
	 */
	public static final String PK_KEY = "pk";


	private String cid;
	/**
	 * 字段名
	 */
	private String name;
	/**
	 *
	 */
	private String type;
	/**
	 *
	 */
	private String notnull;
	/**
	 *
	 */
	private String dflt_value;
	/**
	 *
	 */
	private String pk;

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNotnull() {
        return notnull;
    }

    public void setNotnull(String notnull) {
        this.notnull = notnull;
    }

    public String getDflt_value() {
        return dflt_value;
    }

    public void setDflt_value(String dflt_value) {
        this.dflt_value = dflt_value;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }
}
