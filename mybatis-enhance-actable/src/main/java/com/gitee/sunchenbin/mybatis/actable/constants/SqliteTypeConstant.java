package com.gitee.sunchenbin.mybatis.actable.constants;

import com.gitee.sunchenbin.mybatis.actable.command.sqlite.SqliteTypeAndLength;

import java.util.HashMap;
import java.util.Map;


/**
 *  用于配置Mysql数据库中类型，并且该类型需要设置几个长度
 *  这里配置多少个类型决定了，创建表能使用多少类型
 *  例如：varchar(1)
 *      decimal(5,2)
 *      datetime
 * @author hanpeng
 * @date 2022/7/23 23:43
 */
public enum SqliteTypeConstant {

    /**
     *
     */
	DEFAULT(null,null,null),
	INTEGER(1, 11, null),
    TEXT(0, null, null),
    REAL(1, 255, null),
	BLOB(0, null, null);

	private Integer lengthCount;
	private Integer lengthDefault;
	private Integer decimalLengthDefault;

	public Integer getLengthCount() {
		return lengthCount;
	}

	public Integer getLengthDefault() {
		return lengthDefault;
	}

	public Integer getDecimalLengthDefault() {
		return decimalLengthDefault;
	}

	SqliteTypeConstant(Integer lengthCount, Integer lengthDefault, Integer decimalLengthDefault){
		this.lengthCount = lengthCount;
		this.lengthDefault = lengthDefault;
		this.decimalLengthDefault = decimalLengthDefault;
	}

	// 获取Sqlite的类型，以及类型需要设置几个长度，如需扩展改变这个对象的值即可
	/**
	 * 获取Sqlite的类型，以及类型需要设置几个长度，这里构建成map的样式
	 * 构建Map(字段名(小写),需要设置几个长度(0表示不需要设置，1表示需要设置一个，2表示需要设置两个))
	 */
	public static Map<String, SqliteTypeAndLength> sqliteTypeAndLengthMap;

	static {
        sqliteTypeAndLengthMap = new HashMap<>();
		for (SqliteTypeConstant type: SqliteTypeConstant.values()) {
            sqliteTypeAndLengthMap.put(type.toString().toLowerCase(), new SqliteTypeAndLength(type.getLengthCount(), type.getLengthDefault(), type.getDecimalLengthDefault(), type.toString().toLowerCase()));
		}
	}

}
